package com.akag.tessocrkrismaaditya.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.akag.tessocrkrismaaditya.DigitsDetector;
import com.akag.tessocrkrismaaditya.R;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageOptions;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class GoogleVisionResultFragment extends Fragment implements CropImageView.OnSetImageUriCompleteListener,
        CropImageView.OnCropImageCompleteListener{

    @BindView(R.id.tvUri)
    TextView tvUri;

    @BindView(R.id.civGoogleVisResult)
    CropImageView civGoogleVisResult;

    @BindView(R.id.ivPreviewCamera)
    ImageView ivPreviewCamera;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tvGoogleVisOcrTextResult)
    TextView tvGoogleVisOcrTextResult;

    private String uri;

    private Handler handler;
    private Runnable handlerTask;

    public GoogleVisionResultFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_google_vision_result, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        uri = ResultFragmentArgs.fromBundle(getArguments()).getFileUri();
        tvUri.setText("Lokasi File : " + uri);
        addListener();
        initCropImage(uri);
//        if(civGoogleVisResult.getVisibility() == View.VISIBLE){
//            cropImage();
//        }
    }

    private void setCropImageViewOptions(CropImageOptions options){
        civGoogleVisResult.setScaleType(options.scaleType);
        civGoogleVisResult.setCropShape(options.cropShape);
        civGoogleVisResult.setGuidelines(options.guidelines);
        civGoogleVisResult.setAspectRatio(options.aspectRatioX, options.aspectRatioY);
        civGoogleVisResult.setFixedAspectRatio(options.fixAspectRatio);
        civGoogleVisResult.setMultiTouchEnabled(options.multiTouchEnabled);
        civGoogleVisResult.setShowCropOverlay(options.showCropOverlay);
        civGoogleVisResult.setShowProgressBar(options.showProgressBar);
        civGoogleVisResult.setAutoZoomEnabled(options.autoZoomEnabled);
        civGoogleVisResult.setMaxZoom(options.maxZoom);
        civGoogleVisResult.setFlippedHorizontally(options.flipHorizontally);
        civGoogleVisResult.setFlippedVertically(options.flipVertically);
    }

    private void setInitialCropRect(){

        //1
//        civGoogleVisResult.setCropRect(new Rect(550, 300, 1800, 310));
        civGoogleVisResult.setAutoZoomEnabled(true);
        // landscape exif
        civGoogleVisResult.setCropRect(new Rect(650, 600, 2200, 601));

        // potrait exif
//        civGoogleVisResult.setCropRect(new Rect(1900, 700, 2000, 2200));
//        civGoogleVisResult.setAspectRatio(30, 10);
    }

    private void resetCropRect(){
        civGoogleVisResult.resetCropRect();
    }

    private void initCropImage(String uri){
        // uri method
//        Uri imageUri = Uri.parse(uri);
        Uri imageUri = Uri.fromFile(new File(uri));
//        Toast.makeText(getActivity(), "imageUri is : " + imageUri, Toast.LENGTH_LONG).show();
        civGoogleVisResult.setImageUriAsync(imageUri);

        // bmp method
//        Bitmap bmp = BitmapFactory.decodeFile(uri);
//
//        try {
//            ExifInterface exif = new ExifInterface(uri);
//            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
//            Matrix matrix = new Matrix();
//
//            // 6 = 90 ; 3 = 180; 8 = 270
////            if(orientation == 6){
////                matrix.postRotate(90);
////            }else if(orientation == 3){
////                matrix.postRotate(180);
////            }else if(orientation == 8){
////                matrix.postRotate(270);
////            }
//
//            if(orientation == 6){
//                matrix.postRotate(360);
//            }else if(orientation == 3){
//                matrix.postRotate(180);
//            }else if(orientation == 8){
//                matrix.postRotate(270);
//            }
//
//            bmp = Bitmap.createBitmap(bmp, 0,0, bmp.getWidth(), bmp.getHeight(), matrix, true);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        civGoogleVisResult.setImageBitmap(bmp);
        setInitialCropRect();
//        setInitialCropRect();
    }

    @OnClick(R.id.btnCrop)
    public void cropImage(){
//        civGoogleVisResult.getCroppedImageAsync();
//        Bitmap croppedImage = civGoogleVisResult.getCroppedImage();
//        civGoogleVisResult.setVisibility(View.GONE);
//        removeListener();
//
//        progressBar.setVisibility(View.VISIBLE);
//        initCroppedImage(croppedImage);
    }

    public void autoCropImage(){
        civGoogleVisResult.getCroppedImageAsync();
    }

    private void initImage(String uri){
        Bitmap bmp = BitmapFactory.decodeFile(uri);

        try {
            ExifInterface exif = new ExifInterface(uri);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
            Matrix matrix = new Matrix();

            if(orientation == 6){
                matrix.postRotate(90);
            }else if(orientation == 3){
                matrix.postRotate(180);
            }else if(orientation == 8){
                matrix.postRotate(270);
            }

            bmp = Bitmap.createBitmap(bmp, 0,0, bmp.getWidth(), bmp.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ivPreviewCamera.setImageBitmap(bmp);
    }

    private void initCroppedImage(Bitmap resultBitmap){
        doGoogleVisionOcr(resultBitmap);
        progressBar.setVisibility(View.GONE);
        ivPreviewCamera.setVisibility(View.VISIBLE);
        ivPreviewCamera.setImageBitmap(resultBitmap);
    }

    public void doGoogleVisionOcr(Bitmap bmp){
        TextRecognizer textRecognizer = new TextRecognizer.Builder(getActivity()).build();

        if(!textRecognizer.isOperational()){
            Toast.makeText(getContext(), "Gagal mengenali text!", Toast.LENGTH_LONG).show();
        }else{
            Frame frame = new Frame.Builder().setBitmap(bmp).build();
            SparseArray<TextBlock> items = textRecognizer.detect(frame);
            StringBuilder stringBuilder = new StringBuilder();
            for(int i=0; i<items.size(); i++){
                TextBlock item = items.valueAt(i);
                stringBuilder.append(item.getValue());
                stringBuilder.append("\n");
            }

            // filter hanya nik saja yang diambil

            tvGoogleVisOcrTextResult.setText(stringBuilder.toString());
        }
    }

    private void addListener(){
//        if(civGoogleVisResult == null){
            civGoogleVisResult.setOnSetImageUriCompleteListener(this);
            civGoogleVisResult.setOnCropImageCompleteListener(this);
//        }
    }

    private void removeListener(){
        if(civGoogleVisResult != null){
            civGoogleVisResult.setOnSetImageUriCompleteListener(null);
            civGoogleVisResult.setOnCropImageCompleteListener(null);
        }
    }

    @Override
    public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {

//        String uri = result.getUri().toString();
//        String urigetPath = result.getUri().getPath();

//        Log.e("URI", uri);
//        Log.e("URI get PATH", urigetPath);

        Toast.makeText(getContext(), "Foto berhasil dicrop. Result = " + result, Toast.LENGTH_LONG).show();
//        Toast.makeText(getContext(), "URI = " + uri, Toast.LENGTH_LONG).show();
//        Toast.makeText(getContext(), "URI get PATH = " + urigetPath, Toast.LENGTH_LONG).show();

        Bitmap resultBitmap = result.getBitmap();
        //exif
//        try {
//            ExifInterface exif = new ExifInterface(uri);
//            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
//            Matrix matrix = new Matrix();
//
//            if(orientation == 6){
//                matrix.postRotate(360);
//            }else if(orientation == 3){
//                matrix.postRotate(180);
//            }else if(orientation == 8){
//                matrix.postRotate(270);
//            }
//
//            resultBitmap = Bitmap.createBitmap(resultBitmap, 0,0, resultBitmap.getWidth(), resultBitmap.getHeight(), matrix, true);
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//            Toast.makeText(getContext(), "NullException " + e.getMessage(), Toast.LENGTH_LONG).show();
//        } catch (IOException e) {
//            e.printStackTrace();
//            Toast.makeText(getContext(), "IOException " + e.getMessage(), Toast.LENGTH_LONG).show();
//        }

//        Matrix matrix = new Matrix();
//        matrix.postRotate(360);
//        resultBitmap = Bitmap.createBitmap(resultBitmap, 0,0, resultBitmap.getWidth(), resultBitmap.getHeight(), matrix, true);

        civGoogleVisResult.setVisibility(View.GONE);
        removeListener();
        progressBar.setVisibility(View.VISIBLE);
        initCroppedImage(resultBitmap);
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {
        if(error == null){
            Toast.makeText(getContext(), "Foto berhasil load", Toast.LENGTH_SHORT).show();
//            cropImage();
        }else{
            Toast.makeText(getContext(), "Foto gagal load : " + error.getMessage(), Toast.LENGTH_LONG).show();
        }

//        Timer timer = new Timer();
//
//        TimerTask task = new TimerTask() {
//            @Override
//            public void run() {
//                cropImage();
//            }
//        };

        handler = new Handler();
        handlerTask = new Runnable() {
            @Override
            public void run() {
                autoCropImage();
            }
        };
        handler.postDelayed(handlerTask, 2000);

//        timer.schedule(task, 5000);
//        cropImage();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        removeListener();
    }
}
