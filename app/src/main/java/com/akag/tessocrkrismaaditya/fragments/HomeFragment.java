package com.akag.tessocrkrismaaditya.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.NavHost;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.akag.tessocrkrismaaditya.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

//    @BindView(R.id.btnGoToOcr)
//    Button btnGoToOcr;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @OnClick(R.id.btnGoToOcr)
    public void gotoOcr(){
        NavDirections action = HomeFragmentDirections.homeToTessFragment();
        NavHostFragment.findNavController(this).navigate(action);
    }

    @OnClick(R.id.btnGoToOcrActivity)
    public void gotoOcrActivity(){
        NavDirections action = HomeFragmentDirections.homeToTessActivity();
//        Navigation.findNavController(this, R.id.nav_host_fragment).navigate(action);
        NavHostFragment.findNavController(this).navigate(action);
    }

    @OnClick(R.id.btnGoToOcrActivity2)
    public void gotoOcrActivity2(){
        NavDirections action = HomeFragmentDirections.homeToTessActivity2();
        NavHostFragment.findNavController(this).navigate(action);
    }

    @OnClick(R.id.btnGoToOcrFragment2)
    public void gotoOcrFragment2(){
        NavDirections action = HomeFragmentDirections.homeToTessFragment2();
        NavHostFragment.findNavController(this).navigate(action);
    }
}
