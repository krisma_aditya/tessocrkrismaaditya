package com.akag.tessocrkrismaaditya.fragments;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.akag.tessocrkrismaaditya.DigitsDetector;
import com.akag.tessocrkrismaaditya.R;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class TensorFlowResultFragment extends Fragment implements CropImageView.OnSetImageUriCompleteListener,
CropImageView.OnCropImageCompleteListener{

    @BindView(R.id.tvUri)
    TextView tvUri;

    @BindView(R.id.ivPreviewCamera)
    ImageView ivPreviewCamera;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.civTFLite)
    CropImageView civTFLite;

    @BindView(R.id.tvTFLiteResult)
    TextView tvTFLiteResult;

    private String uri;

    private Handler handler;
    private Runnable handlerTask;

    //tensorflow test
    private DigitsDetector mnistClassifier;

    public TensorFlowResultFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tensor_flow_result, container, false);
        ButterKnife.bind(this, view);

        mnistClassifier = new DigitsDetector(getActivity());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        uri = TensorFlowResultFragmentArgs.fromBundle(getArguments()).getFileUri();
        tvUri.setText("Lokasi File : " + uri);
        addListener();
        initCropImage(uri);
    }

    private void addListener(){
        civTFLite.setOnSetImageUriCompleteListener(this);
        civTFLite.setOnCropImageCompleteListener(this);
    }

    private void removeListener(){
        if(civTFLite != null){
            civTFLite.setOnSetImageUriCompleteListener(null);
            civTFLite.setOnCropImageCompleteListener(null);
        }
    }

    private void setInitialCropRect(){

        //1
//        civTFLite.setCropRect(new Rect(550, 300, 1800, 310));
        civTFLite.setAutoZoomEnabled(true);
        // landscape exif
        civTFLite.setCropRect(new Rect(650, 600, 2200, 601));

        // potrait exif
//        civTFLite.setCropRect(new Rect(1900, 700, 2000, 2200));
//        civTFLite.setAspectRatio(30, 10);
    }

    private void resetCropRect(){
        civTFLite.resetCropRect();
    }

    private void initCropImage(String uri){
        // uri method
        Uri imageUri = Uri.fromFile(new File(uri));
        civTFLite.setImageUriAsync(imageUri);
        setInitialCropRect();
    }

    @OnClick(R.id.btnCrop)
    public void cropImage(){
        civTFLite.getCroppedImageAsync();
//        Bitmap croppedImage = civGoogleVisResult.getCroppedImage();
//        civGoogleVisResult.setVisibility(View.GONE);
//        removeListener();
//
//        progressBar.setVisibility(View.VISIBLE);
//        initCroppedImage(croppedImage);
    }

    public void autoCropImage(){
        civTFLite.getCroppedImageAsync();
    }

    private void initCroppedImage(Bitmap resultBitmap){
        doOcrTfLite(resultBitmap);
        progressBar.setVisibility(View.GONE);
        ivPreviewCamera.setVisibility(View.VISIBLE);
        ivPreviewCamera.setImageBitmap(resultBitmap);
    }

    // ocr using tflite
    public void doOcrTfLite(Bitmap bitmap){
        int digit = mnistClassifier.classify(bitmap);

        if(digit >= 0){
            tvTFLiteResult.setText("DIGIT = " + digit);
        }else{
            tvTFLiteResult.setText("Gagal mengenali gambar");
        }
    }


    @Override
    public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {
        Toast.makeText(getContext(), "Foto berhasil dicrop. Result = " + result, Toast.LENGTH_LONG).show();

        Bitmap resultBitmap = result.getBitmap();
        civTFLite.setVisibility(View.GONE);
        removeListener();
        progressBar.setVisibility(View.VISIBLE);
        initCroppedImage(resultBitmap);
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {
        if(error == null){
            Toast.makeText(getContext(), "Foto berhasil load", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(getContext(), "Foto gagal load : " + error.getMessage(), Toast.LENGTH_LONG).show();
        }

        handler = new Handler();
        handlerTask = new Runnable() {
            @Override
            public void run() {
                autoCropImage();
            }
        };
//        handler.postDelayed(handlerTask, 5000);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        removeListener();
    }
}
