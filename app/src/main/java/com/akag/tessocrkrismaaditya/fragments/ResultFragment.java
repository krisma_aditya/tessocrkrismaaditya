package com.akag.tessocrkrismaaditya.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.akag.tessocrkrismaaditya.R;
import com.googlecode.tesseract.android.TessBaseAPI;
import com.theartofdev.edmodo.cropper.CropImageOptions;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.net.URI;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ResultFragment extends Fragment implements CropImageView.OnSetImageUriCompleteListener,
        CropImageView.OnCropImageCompleteListener {

    @BindView(R.id.tvUri)
    TextView tvUri;

    @BindView(R.id.civResult)
    CropImageView civResult;

    @BindView(R.id.ivPreviewCamera)
    ImageView ivPreviewCamera;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tvTessOcrTextResult)
    TextView tvTessOcrTextResult;

    private String uri;
    private Handler handler;
    private Runnable handlerTask;

    public ResultFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_result, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        uri = ResultFragmentArgs.fromBundle(getArguments()).getFileUri();
        tvUri.setText(uri);
//        init(uri);
        addListener();
        initCropImage(uri);
    }

    private void setCropImageViewOptions(CropImageOptions options){
        civResult.setScaleType(options.scaleType);
        civResult.setCropShape(options.cropShape);
        civResult.setGuidelines(options.guidelines);
        civResult.setAspectRatio(options.aspectRatioX, options.aspectRatioY);
        civResult.setFixedAspectRatio(options.fixAspectRatio);
        civResult.setMultiTouchEnabled(options.multiTouchEnabled);
        civResult.setShowCropOverlay(options.showCropOverlay);
        civResult.setShowProgressBar(options.showProgressBar);
        civResult.setAutoZoomEnabled(options.autoZoomEnabled);
        civResult.setMaxZoom(options.maxZoom);
        civResult.setFlippedHorizontally(options.flipHorizontally);
        civResult.setFlippedVertically(options.flipVertically);
    }

    private void setInitialCropRect(){
//        civResult.setCropRect(new Rect(100, 300, 500, 1200));
        civResult.setCropRect(new Rect(650, 600, 2200, 601));
    }

    private void resetCropRect(){
        civResult.resetCropRect();
    }

    private void initCropImage(String uri){
        // uri method
        Uri imageUri = Uri.fromFile(new File(uri));
        civResult.setImageUriAsync(imageUri);
        setInitialCropRect();

        // bmp method
//        Bitmap bmp = BitmapFactory.decodeFile(uri);
//
//        try {
//            ExifInterface exif = new ExifInterface(uri);
//            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
//            Matrix matrix = new Matrix();
//
//            if(orientation == 6){
//                matrix.postRotate(90);
//            }else if(orientation == 3){
//                matrix.postRotate(180);
//            }else if(orientation == 8){
//                matrix.postRotate(270);
//            }
//
//            bmp = Bitmap.createBitmap(bmp, 0,0, bmp.getWidth(), bmp.getHeight(), matrix, true);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        setInitialCropRect();
//        civResult.setImageBitmap(bmp);
    }

    @OnClick(R.id.btnCrop)
    public void cropImage(){
//        civResult.getCroppedImageAsync();
//        Bitmap croppedImage = civResult.getCroppedImage();
//        civResult.setVisibility(View.GONE);
//        removeListener();
//
//        progressBar.setVisibility(View.VISIBLE);
//        initCroppedImage(croppedImage);
    }

    public void autoCropImage(){
        civResult.getCroppedImageAsync();
    }

    private void initImage(String uri){
        Bitmap bmp = BitmapFactory.decodeFile(uri);

        try {
            ExifInterface exif = new ExifInterface(uri);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
            Matrix matrix = new Matrix();

            if(orientation == 6){
                matrix.postRotate(90);
            }else if(orientation == 3){
                matrix.postRotate(180);
            }else if(orientation == 8){
                matrix.postRotate(270);
            }

            bmp = Bitmap.createBitmap(bmp, 0,0, bmp.getWidth(), bmp.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ivPreviewCamera.setImageBitmap(bmp);
    }

    private void initCroppedImage(Bitmap resultBitmap){
        ocr(resultBitmap);
        progressBar.setVisibility(View.GONE);
        ivPreviewCamera.setVisibility(View.VISIBLE);
        ivPreviewCamera.setImageBitmap(resultBitmap);
    }

    public void ocr(Bitmap bmp){
        TessBaseAPI tessBaseAPI = new TessBaseAPI();

//        String path = getExternalFilesDir(null).getAbsolutePath() + "/tesseract/";
        String path = Environment.getExternalStorageDirectory() + "/tesseract";

        Toast.makeText(getActivity(), "Tesseract path is : " + path, Toast.LENGTH_LONG).show();

        tessBaseAPI.init(path,"eng");
//        tessBaseAPI.setPageSegMode(TessBaseAPI.PageSegMode.PSM_SINGLE_LINE);
        tessBaseAPI.setPageSegMode(TessBaseAPI.PageSegMode.PSM_AUTO);
        tessBaseAPI.setImage(bmp);

        String result = tessBaseAPI.getUTF8Text();

        System.out.println("OCR RESULT = " + result);
//        Toast.makeText(getActivity(), "OCR RESULT : " + result, Toast.LENGTH_LONG).show();
        tvTessOcrTextResult.setText(result);
    }

    private void addListener(){
//        if(civResult == null){
            civResult.setOnSetImageUriCompleteListener(this);
            civResult.setOnCropImageCompleteListener(this);
//        }
    }

    private void removeListener(){
        if(civResult != null){
            civResult.setOnSetImageUriCompleteListener(null);
            civResult.setOnCropImageCompleteListener(null);
        }
    }

    @Override
    public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {
        Bitmap croppedImage = result.getBitmap();
        civResult.setVisibility(View.GONE);
        removeListener();

        progressBar.setVisibility(View.VISIBLE);
        initCroppedImage(croppedImage);
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {
        if(error == null){
//            Toast.makeText(getActivity(), "Foto berhasil load", Toast.LENGTH_SHORT).show();
        }else{
//            Toast.makeText(getActivity(), "Foto gagal load : " + error.getMessage(), Toast.LENGTH_LONG).show();
        }

        handler = new Handler();
        handlerTask = new Runnable() {
            @Override
            public void run() {
                autoCropImage();
            }
        };
        handler.postDelayed(handlerTask, 2000);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        removeListener();
    }
}
