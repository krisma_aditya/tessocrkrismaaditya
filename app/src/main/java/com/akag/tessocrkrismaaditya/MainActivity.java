package com.akag.tessocrkrismaaditya;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.akag.tessocrkrismaaditya.fragments.GoogleVisionFragmentDirections;
import com.akag.tessocrkrismaaditya.fragments.GoogleVisionResultFragmentDirections;
import com.akag.tessocrkrismaaditya.fragments.HomeFragmentDirections;
import com.akag.tessocrkrismaaditya.fragments.ResultFragmentDirections;
import com.akag.tessocrkrismaaditya.fragments.SearchFragmentDirections;
import com.akag.tessocrkrismaaditya.fragments.TensorFlowFragmentDirections;
import com.akag.tessocrkrismaaditya.fragments.TessFragment2Directions;
import com.akag.tessocrkrismaaditya.fragments.TessFragmentDirections;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class MainActivity extends BaseActivity {

    @BindView(R.id.etSearchBar)
    EditText etSearchBar;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tabLayout.addOnTabSelectedListener(tabSelectedListener);
    }

    TabLayout.OnTabSelectedListener tabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            String tabName = tab.getText().toString();
            int tabNum = tab.getPosition();
            CharSequence label = Navigation.findNavController(MainActivity.this, R.id.nav_host_fragment).getCurrentDestination().getLabel();

            switch (tabNum){
                case 0:
                    if(label.equals("fragment_tensor_flow")){
                        NavDirections action = TensorFlowFragmentDirections.tensorToHomeFragment();
                        Navigation.findNavController(MainActivity.this, R.id.nav_host_fragment).navigate(action);
                    }else if(label.equals("fragment_google_vision")){
                        NavDirections action0 = GoogleVisionFragmentDirections.googleToHomeFragment();
                        Navigation.findNavController(MainActivity.this, R.id.nav_host_fragment).navigate(action0);
                    }
                    break;
                case 1:
                    if(label.equals("fragment_home")){
                        NavDirections action = HomeFragmentDirections.homeToTensorFlowFragment();
                        Navigation.findNavController(MainActivity.this, R.id.nav_host_fragment).navigate(action);
                    }else if(label.equals("fragment_google_vision")){
                        NavDirections action0 = GoogleVisionFragmentDirections.googleToTensorFlowFragment();
                        Navigation.findNavController(MainActivity.this, R.id.nav_host_fragment).navigate(action0);
                    }
                    break;
                case 2:
                    if(label.equals("fragment_home")){
                        NavDirections action = HomeFragmentDirections.homeToGoogleVisionFragment();
                        Navigation.findNavController(MainActivity.this, R.id.nav_host_fragment).navigate(action);
                    }else if(label.equals("fragment_tensor_flow")){
                        NavDirections action0 = TensorFlowFragmentDirections.tensorToGoogleVisionFragment();
                        Navigation.findNavController(MainActivity.this, R.id.nav_host_fragment).navigate(action0);
                    }
                    break;
            }

        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
//            case R.id.searchBar:
//                return true;
//            case R.id.favorite:
//                goToSearchPage();
//                goToFavorite();
//                Toast.makeText(getApplicationContext(), "Favorite Pressed", Toast.LENGTH_SHORT).show();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnTextChanged(R.id.etSearchBar)
    public void goToSearchPage(){
//        Toast.makeText(getApplicationContext(), "Touched", Toast.LENGTH_SHORT).show();
//        SearchActivity.startActivity(this);
        CharSequence label = Navigation.findNavController(this, R.id.nav_host_fragment).getCurrentDestination().getLabel();

        if(label.equals("fragment_search")){
            search();
        }else{
//            NavDirections action = HomeFragmentDirections.homeToSearchFragment();
            NavDirections action = HomeFragmentDirections.homeToSearchFragment();
            Navigation.findNavController(this, R.id.nav_host_fragment).navigate(action);
        }
    }

    private void search() {
        String query = etSearchBar.getText().toString();
        Toast.makeText(getApplicationContext(), "Search performed. Query : " + query, Toast.LENGTH_SHORT).show();
    }

//    @OnClick(R.id.tiTess)
//    public void changePageToTessHome(){
//        CharSequence label = Navigation.findNavController(this, R.id.nav_host_fragment).getCurrentDestination().getLabel();
//        switch(label.toString()){
//            case "fragment_tensor_flow":
//                NavDirections action = TensorFlowFragmentDirections.tensorToHomeFragment();
//                Navigation.findNavController(this, R.id.nav_host_fragment).navigate(action);
//                break;
//            case "fragment_google_vision":
//                NavDirections action0 = GoogleVisionFragmentDirections.googleToHomeFragment();
//                Navigation.findNavController(this, R.id.nav_host_fragment).navigate(action0);
//                break;
//        }
//    }
//
//    @OnClick(R.id.tiTensor)
//    public void changePageToTensor(){
//        CharSequence label = Navigation.findNavController(this, R.id.nav_host_fragment).getCurrentDestination().getLabel();
//
//        switch(label.toString()){
//            case "fragment_home":
//                NavDirections action = HomeFragmentDirections.homeToTensorFlowFragment();
//                Navigation.findNavController(this, R.id.nav_host_fragment).navigate(action);
//                break;
//            case "fragment_google_vision":
//                NavDirections action0 = GoogleVisionFragmentDirections.googleToTensorFlowFragment();
//                Navigation.findNavController(this, R.id.nav_host_fragment).navigate(action0);
//                break;
//        }
//    }
//
//    @OnClick(R.id.tiGoogleVis)
//    public void changePageToGoogle(){
//        CharSequence label = Navigation.findNavController(this, R.id.nav_host_fragment).getCurrentDestination().getLabel();
//
//        switch(label.toString()){
//            case "fragment_home":
//                NavDirections action = HomeFragmentDirections.homeToGoogleVisionFragment();
//                Navigation.findNavController(this, R.id.nav_host_fragment).navigate(action);
//                break;
//            case "fragment_tensor_flow":
//                NavDirections action0 = TensorFlowFragmentDirections.tensorToGoogleVisionFragment();
//                Navigation.findNavController(this, R.id.nav_host_fragment).navigate(action0);
//                break;
//        }
//    }

    @Override
    public void onBackPressed() {
        CharSequence label = Navigation.findNavController(this, R.id.nav_host_fragment).getCurrentDestination().getLabel();

        switch(label.toString()) {
            case "fragment_search":
                NavDirections action = SearchFragmentDirections.searchToHomeFragment();
                Navigation.findNavController(this, R.id.nav_host_fragment).navigate(action);
                break;
            case "fragment_tess":
                NavDirections action0 = TessFragmentDirections.tessToHomeFragment();
                Navigation.findNavController(this, R.id.nav_host_fragment).navigate(action0);
                break;
            case "fragment_tess2":
                NavDirections captureToHomeAction = TessFragment2Directions.tessFragment2ToHomeFragment();
                Navigation.findNavController(this, R.id.nav_host_fragment).navigate(captureToHomeAction);
                break;
            case "fragment_result":
                NavDirections resultBackToCaptureAction = ResultFragmentDirections.resultFragmentToTessFragment2();
                Navigation.findNavController(this, R.id.nav_host_fragment).navigate(resultBackToCaptureAction);
                break;
            case "fragment_google_vision_result":
                NavDirections resultBackToGoogleVisCaptureAction = GoogleVisionResultFragmentDirections.googleVisionResultFragmentToGoogleVisionFragment();
                Navigation.findNavController(this, R.id.nav_host_fragment).navigate(resultBackToGoogleVisCaptureAction);
                break;
            default:
                super.onBackPressed();
        }
    }
}
