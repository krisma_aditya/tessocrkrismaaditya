package com.akag.tessocrkrismaaditya;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.instantapps.InstantApps;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

//        showInstallPrompt();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    // check apakah aplikasi mendukung instant app
    private void showInstallPrompt(){
        Intent postInstall = new Intent(Intent.ACTION_MAIN)
                .addCategory(Intent.CATEGORY_DEFAULT)
                .setPackage(getPackageName());

        InstantApps.showInstallPrompt(BaseActivity.this, postInstall, 1, null);
    }
}
